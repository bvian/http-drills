const http = require('http');
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');

const PORT = process.env.PORT || 3030;

const server = http.createServer((req, res) => {

    const requestUrl = req.url;
    const statusCode = Number(requestUrl.split("/").slice(-1));
    const delayParameter = statusCode;

    if (requestUrl === "/html" && req.method === "GET") {

        fs.readFile("index.html", "utf-8", (err, data) => {
            if (err) {
                console.error(err);
                res.writeHead(500, { "Content-Type": "application/json" });
                const errorMessage = {
                    message: "Internal Server Error",
                    error: "Invalid request, cannot read html file"
                };
                res.write(JSON.stringify(errorMessage));

            } else {
                res.writeHead(200, { "Content-Type": "text/html" });
                res.write(data);
            }
            res.end();
        });

    } else if (requestUrl === "/json" && req.method === "GET") {

        fs.readFile("data.json", "utf-8", (err, data) => {
            if (err) {
                console.error(err);
                res.writeHead(500, { "Content-Type": "application/json" });
                const errorMessage = {
                    message: "Internal Server Error",
                    error: "Invalid request, cannot read json file"
                };
                res.write(JSON.stringify(errorMessage));

            } else {
                res.writeHead(200, { "Content-Type": "application/json" });
                res.write(data);
            }
            res.end();
        });

    } else if (requestUrl === "/uuid" && req.method === "GET") {
        const newId = uuidv4();
        const uuidObject = {
            uuid: newId
        };

        res.writeHead(200, { "Content-Type": "application/json" });
        res.write(JSON.stringify(uuidObject));
        res.end();

    } else if (requestUrl === `/status/${statusCode}` && req.method === "GET") {

        const httpStatusCode = http.STATUS_CODES[statusCode.toString()];

        if (httpStatusCode === undefined) {
            res.writeHead(500, { "Content-Type": "application/json" });
            const errorMessage = {
                message: "Internal Server Error",
                error: "Status code is invalid"
            };
            res.write(JSON.stringify(errorMessage));

        } else {
            res.writeHead(statusCode, { "Content-Type": "text/plain" });
            res.write(`Status Code Response: ${httpStatusCode}`);
        }
        res.end();

    } else if (requestUrl === `/delay/${delayParameter}` && req.method === "GET") {

        if (delayParameter < 0) {
            res.writeHead(500, { "Content-Type": "application/json" });
            const errorMessage = {
                message: "Internal Server Error",
                error: "Invalid request, cannot take negative value in parameter"
            };
            res.write(JSON.stringify(errorMessage));
            res.end();

        } else {

            setTimeout(() => {
                res.writeHead(200, { "Content-Type": "text/plain" });
                res.write(`Response delayed for ${delayParameter} seconds`);
                res.end();
            }, delayParameter * 1000);
        }

    } else {
        res.writeHead(500, { "Content-Type": "application/json" });
        const errorMessage = {
            message: "Internal Server Error",
            error: "Invalid request"
        };
        res.write(JSON.stringify(errorMessage));
        res.end();
    }
});

server.listen(PORT, () => {
    console.log(`server started in port ${PORT}`);
});